<?php 
    session_start();

    require 'bdd.php';

    if(isset($_POST['connexion']))
    {
        $usernameconnect = htmlspecialchars($_POST['usernameconnect']);
        $passwordconnect = md5($_POST['passwordconnect']);

        if(!empty($usernameconnect) AND !empty($passwordconnect)) 
        {
            $requser = $bdd->prepare("SELECT * FROM users WHERE username = ? AND password = ?");
            $requser->execute(array($usernameconnect, $passwordconnect));
            $userexist = $requser->rowCount();

            if($userexist == 1) 
            {
                $userinfo = $requser->fetch();
                $_SESSION['id'] = $userinfo['id'];
                $_SESSION['username'] = $userinfo['username'];
                $_SESSION['email'] = $userinfo['email'];
                $_SESSION['solde'] = $userinfo['solde'];
                header("Location: index.php");
            } 
            else 
            {
                $erreur = "Mauvaises Informations !";
            }
        } 
        else {
            $erreur = "Tous les champs ne sont pas rempli !";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/app.css">
        <title>Connexion</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="index.php">A propos</a></li>
                    </ul>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                </div>
            </nav>
        <div class="login_form">

            <h2>Connexion</h2>

            <form method="POST" action="">
                <div class="login_box">
                    <input type="text" placeholder="Pseudo" name="usernameconnect" id="usernameconnect">
                </div>

                <div class="login_box">
                    <input type="password" placeholder="Mot de passe" name="passwordconnect" id="passwordconnect">
                </div>
                
                <input class="submit" type="submit" name="connexion" value="Se connecter">
            </form>

            <p>Pas de compte ? Inscrivez-vous <a href="register.php">ici</a> !</p>

            <p style="color: white;">
                <?php 
                    if(isset($erreur)) {
                        echo $erreur;
                    }
                ?>
            </p>

        </div>
    </body>
</html>