<?php 
    session_start();

    require 'bdd.php';

    if (isset($_SESSION['username'])) {
        $sUsername = $_SESSION['username'];
    }
    else {
        $sUsername = NULL;
    }

    if(isset($_POST['upload']))
    {
        $name = htmlspecialchars($_POST['name']);
        $date = htmlspecialchars($_POST['date']);
        $prix = htmlspecialchars($_POST['prix']);

        if(!empty($name) AND !empty($date)) 
        {
            $insertuser = $bdd->prepare("INSERT INTO nft(nft_name, author, date_publication, prix, img) VALUES(?,?,?,?,?)");
            $insertuser->execute(array($name, $_SESSION['username'], $date, $prix, "default.png"));
            header('Location: index.php');
        }
        else {
            $erreur = "Tous les champs ne sont pas rempli !";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/app.css">
        <title>NFT</title>
    </head>
    <body>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="index.php">A propos</a></li>
                    </ul>

                    <?php 
                        if($sUsername != null) {
                    ?>
                        <li><a href="account.php?id=<?php echo  $_SESSION['id']; ?>" class="nav-link px-2 text-white text-decoration-none"><?php echo  $_SESSION['username']; ?></a></li>
                    </ul>

                    <div class="text-end">
                        <button type="button" class="btn btn-warning"><a href="logout.php" class="text-white text-decoration-none">Déconnexion</a></button>
                    <?php
                        } else {
                    ?>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                    <?php   
                        }
                    ?>
                </div>
            </nav>
        <div class="login_form">

            <h2>Votre NFT</h2>

            <form method="POST" action="">
                <div class="login_box">
                    <input type="text" placeholder="Nom du NFT" name="name" id="name">
                </div>

                <div class="login_box">
                    <input type="text" placeholder="Date de publication" name="date" id="date">
                </div>

                <div class="login_box">
                    <input type="text" placeholder="Prix" name="prix" id="prix">
                </div>
                
                <input class="submit" type="submit" name="upload" value="Envoyer">
            </form>

            <p><a href="index.php">Accueil</a></p>

            <p style="color: white;">
                <?php 
                    if(isset($erreur)) {
                        echo $erreur;
                    }
                ?>
            </p>

        </div>
    </body>
</html>