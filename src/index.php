<?php 
    session_start();

    require 'bdd.php';

    if (isset($_SESSION['username'])) {
        $sUsername = $_SESSION['username'];
    }
    else {
        $sUsername = NULL;
    }

    $querry = $bdd->prepare("SELECT * FROM nft ORDER BY date_publication DESC");
    $querry->execute();
    $fetch = $querry->fetchAll();

    $i = 0;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/app.css">
        <title>Accueil</title>
    </head>

    <body>
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link" href="transactions.php">Transactions</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="index.php">A propos</a></li>
                    </ul>

                    <?php 
                        if($sUsername != null) {
                    ?>
                        <li><a href="account.php?id=<?php echo  $_SESSION['id']; ?>" class="nav-link px-2 text-white text-decoration-none"><?php echo  $_SESSION['username']; ?></a></li>
                    </ul>

                    <div class="text-end">
                        <button type="button" class="btn btn-warning"><a href="logout.php" class="text-white text-decoration-none">Déconnexion</a></button>
                    <?php
                        } else {
                    ?>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                    <?php   
                        }
                    ?>
                </div>
            </nav>

            <div class="home-pres jumbotron p-3 p-md-5 text-white rounded bg-dark">
                <div class="col-md-6 px-0">
                <h1 class="display-4 font-italic">Bienvenue dans NFTGANG</h1>
                <p class="lead my-3">Ici ya tou, des nft de la crypto et voila !</p>
                </div>
            </div>

            <h2>Dernier NFT Ajouté :</h2>

            <?php 
                while ($fetch && $i<count($fetch))
                {
            ?>      

            <div class="card mb-2" style="width: 15rem;">
                <img class="card-img-top" src="img/testnft.jpg">
                <div class="card-body">
                <h3 class="card-title"><?=ucwords(utf8_encode($fetch[$i]['nft_name'])); ?></h3>
                <h5 class="card-title">Propriétaire : <?=ucwords(utf8_encode($fetch[$i]['author'])); ?></h5>
                <h5 class="card-title">Prix : <?=ucwords(utf8_encode($fetch[$i]['prix'])); ?></h5>
                <h5 class="card-title"><?php echo utf8_encode($fetch[$i]['date_publication']); ?></h5>
                <h5 class="card-title"><a href="buy-nft.php">Acheter</a></h5>
                </div>
            </div>

            <?php 
                $i++;
                }  
            ?>  
        </div>
    </body>
</html>