<?php 
    session_start();

    require 'bdd.php';

    if (isset($_SESSION['username'])) {
        $sUsername = $_SESSION['username'];
    }
    else {
        $sUsername = NULL;
    }

    $querry = $bdd->prepare("SELECT * FROM transactions ORDER BY date_transaction DESC");
    $querry->execute();
    $fetch = $querry->fetchAll();

    $i = 0;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/app.css">
        <title>Transactions</title>
    </head>

    <body>
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link" href="transactions.php">Transactions</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="index.php">A propos</a></li>
                    </ul>

                    <?php 
                        if($sUsername != null) {
                    ?>
                        <li><a href="account.php?id=<?php echo  $_SESSION['id']; ?>" class="nav-link px-2 text-white text-decoration-none"><?php echo  $_SESSION['username']; ?></a></li>
                    </ul>

                    <div class="text-end">
                        <button type="button" class="btn btn-warning"><a href="logout.php" class="text-white text-decoration-none">Déconnexion</a></button>
                    <?php
                        } else {
                    ?>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                    <?php   
                        }
                    ?>
                </div>
            </nav>

            <h2>Dernière transactions : </h2>       
        </div>
    
        <div class="table-responsive">
            <table class="table table-striped table-sm">
            <thead>
                <tr>
                <th scope="col">#</th>
                <th scope="col">Date</th>
                <th scope="col">Nom du nft</th>
                <th scope="col">Propriétaire actuel</th>
                <th scope="col">Hash</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    while ($fetch && $i<count($fetch))
                    {
                ?>
                <tr>
                <td><?=ucwords(utf8_encode($fetch[$i]['id'])); ?></td>
                <td><?=ucwords(utf8_encode($fetch[$i]['date_transaction'])); ?></td>
                <td><?=ucwords(utf8_encode($fetch[$i]['name_nft'])); ?></td>
                <td><?php echo utf8_encode($fetch[$i]['name_proprio']); ?></td>
                <td><?php echo utf8_encode($fetch[$i]['hash']); ?></td>
                </tr>
                <?php 
                    $i++;
                    }  
                ?> 
            </tbody>
            </table>
        </div> 
    </body>
</html>