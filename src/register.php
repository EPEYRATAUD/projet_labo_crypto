<?php 
    require 'bdd.php';

    $inscriptioncheck = false;

    if(isset($_POST['inscription']))
    {
        $username = htmlspecialchars($_POST['username']);    
        $email = htmlspecialchars($_POST['email']);
        $password = md5($_POST['password']);   
        $confirmpassword = md5($_POST['confirmpassword']);  

        if(!empty($_POST['username']) AND !empty($_POST['email']) AND !empty($_POST['password']) AND !empty($_POST['confirmpassword'])) 
        {   
            $usernamelength = strlen($username);
            if($usernamelength <= 255) 
            {
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) 
                {
                    $reqemail = $bdd->prepare("SELECT * FROM users WHERE email = ?");
                    $reqemail->execute(array($email));
                    $emailexist = $reqemail->rowCount();

                    $requsername = $bdd->prepare("SELECT * FROM users WHERE username = ?");
                    $requsername->execute(array($username));
                    $usernameexist = $requsername->rowCount();

                    if($usernameexist == 0){
                        if($emailexist == 0) 
                        {
                            if($password == $confirmpassword) 
                            {
                                $insertuser = $bdd->prepare("INSERT INTO users(username, email, password, solde) VALUES(?,?,?,?)");
                                $insertuser->execute(array($username, $email, $password, 0));
                                header('Location: login.php');
                            } else {
                                $erreur = "Mot de passe ne corespondent pas !";
                            }
                        } else 
                        {
                            $erreur = "Adresse mail déjà utilisée !"; 
                        }
                    } else 
                    {
                        $erreur = "Pseudo déjà utilisé !";
                    }
                }
            } else {
                $erreur = "Pseudo trop long !";
            }
        } 
        else 
        {
            $erreur = "Tous les champs doivent être remplis !";
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/app.css">
    <title>Inscription</title>
</head>
<body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
        <a class="navbar-brand" href="index.php">NFTGANG</a>
            <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                    <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                    <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                    <li class="nav-item"><a class="nav-link disabled" href="index.php">A propos</a></li>
                </ul>
                    <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                    <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
            </div>
        </nav>
    <div class="login_form">
        <form method="POST" action="">
            <h2>Inscription</h2>
            <div class="login_box">
                <input type="text" placeholder="Pseudo" name="username" id="username" value="<?php if(isset($username)) {echo $username;}?>">
            </div>

            <div class="login_box">
                <input type="email" placeholder="Email" name="email" id="email" value="<?php if(isset($email)) {echo $email;}?>">
            </div>

            <div class="login_box">
                <input type="password" placeholder="Mot de passe" name="password" id="password">
            </div>

            <div class="login_box">
                <input type="password" placeholder="Mot de passe" name="confirmpassword" id="confirmpassword">
            </div>
            
                <input class="submit" type="submit" name="inscription" value="Envoyer">
        </form>
        <p>Déjà un compte ? Connectez-vous <a href="login.php">ici</a> !</p>

        <p style="color: white;">
            <?php 
                if(isset($erreur)) {
                    echo $erreur;
                }
            ?>
        </p>
    </div>
</body>
</html>