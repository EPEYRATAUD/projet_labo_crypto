
<?php 
    session_start();

    require 'bdd.php';

    if (isset($_SESSION['username'])) {
        $sUsername = $_SESSION['username'];
    }
    else {
        $sUsername = NULL;
    }


    if(empty($_SESSION['id']))
    {
        header("Location: login.php");
    }

    if(isset($_GET['id']) AND $_GET['id'] > 0)
    {
        $getid = intval($_GET['id']);
        $requser = $bdd->prepare('SELECT * FROM users WHERE id = ?');
        $requser->execute(array($getid));
        $userinfo = $requser->fetch();
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="css/app.css">
        <title>Profil</title>
    </head>
    <body>
    <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link" href="transactions.php">Transactions</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="#">A propos</a></li>
                    </ul>

                    <?php 
                        if($sUsername != null) {
                    ?>
                        <li><a href="account.php?id=<?php echo  $_SESSION['id']; ?>" class="nav-link px-2 text-white text-decoration-none"><?php echo  $_SESSION['username']; ?></a></li>
                    </ul>

                    <div class="text-end">
                        <button type="button" class="btn btn-warning"><a href="logout.php" class="text-white text-decoration-none">Déconnexion</a></button>
                    <?php
                        } else {
                    ?>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                    <?php   
                        }
                    ?>
                </div>
            </nav>

        <h1>Profil</h1>

        <div class="card-profil">
                <div class="card-body">
                    <div class="card-date">
                        <time><a style="text-decoration: underline;" href="editionprofile.php">Editer le profil</a> </time>
                    </div>

                    <div class="card-title">
                        <h3>Pseudo : <?php echo  $_SESSION['username'];?></h3>
                        <h3>Email : <?php echo  $_SESSION['email'];?></h3>
                        <h3>Solde : <?php echo  $_SESSION['solde'];?></h3>
                    </div>
                </div>
            </a>
        </div>

        <div class="title-with-border">
            <h1>Vos NFT</h1>
        </div>

        <?php 
            $user = $_SESSION['username'];
            $querry = $bdd->prepare("SELECT * FROM nft WHERE author = '$user' ORDER BY date_publication DESC");
            $querry->execute();
            $fetch = $querry->fetchAll();
            
        
            $i = 0;

            while ($fetch && $i<count($fetch))
            {
        ?>      

            <div class="card mb-2" style="width: 15rem;">
                <img class="card-img-top" src="img/testnft.jpg">
                <div class="card-body">
                <h3 class="card-title"><?=ucwords(utf8_encode($fetch[$i]['nft_name'])); ?></h3>
                <h5 class="card-title">Auteur : <?=ucwords(utf8_encode($fetch[$i]['author'])); ?></h5>
                <h5 class="card-title">Prix : <?=ucwords(utf8_encode($fetch[$i]['prix'])); ?></h5>
                <time><?php echo utf8_encode($fetch[$i]['date_publication']); ?></time>
                </div>
                <!-- <?php if($fetch[$i]['author'] == $_SESSION['id'] OR $_SESSION['username'] == "admin") { ?>
                        <a class="button-suppr" href="suppr-article.php?id=<?php echo utf8_encode($fetch[$i]['articleId']); ?>"><span>Supprimer<span></a>
                <?php }?> -->
            </div>

            <?php 
                $i++;
                }  
            ?>  

    </body>
</html>

<?php
}
?>