<?php 
    session_start();

    require 'bdd.php';

    if (isset($_SESSION['username'])) {
        $sUsername = $_SESSION['username'];
    }
    else {
        $sUsername = NULL;
    }

    if(empty($_SESSION['id']))
    {
        header("Location: login.php");
    }
    
    $soldeuser = $_SESSION['solde'];

    if(isset($_POST['submitbutton']))
    {
      $soldeuser = $soldeuser + 100;
      $iduser = $_SESSION['id'];
      $givesolde = $bdd->exec("UPDATE users SET solde=$soldeuser WHERE id='$iduser'");
      session_destroy();
      session_start();
      header('Location: index.php');
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<title>Ajouter des fonds</title>
</head>
<body>
    <header class="p-3 bg-dark text-white">
        <div class="container">
        <div class="container">
            <nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
                <a class="navbar-brand" href="index.php">NFTGANG</a>
                <div class="collapse navbar-collapse" id="navbarsExampleDefault">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active"><a class="nav-link" href="index.php">Accueil</a></li>
                        <li class="nav-item"><a class="nav-link" href="upload-nft.php">NFT</a></li>
                        <li class="nav-item"><a class="nav-link" href="add-funds.php">Solde</a></li>
                        <li class="nav-item"><a class="nav-link" href="transactions.php">Transactions</a></li>
                        <li class="nav-item"><a class="nav-link disabled" href="index.php"><?php echo $_SESSION['solde']; ?>A propos</a></li>
                    </ul>

                    <?php 
                        if($sUsername != null) {
                    ?>
                        <li><a href="account.php?id=<?php echo  $_SESSION['id']; ?>" class="nav-link px-2 text-white text-decoration-none"><?php echo  $_SESSION['username']; ?></a></li>
                    </ul>

                    <div class="text-end">
                        <button type="button" class="btn btn-warning"><a href="logout.php" class="text-white text-decoration-none">Déconnexion</a></button>
                    <?php
                        } else {
                    ?>
                        <button type="button" class="btn btn-warning"><a href="register.php" class="text-white text-decoration-none">Inscription</a></button>
                        <button type="button" class="btn btn-outline-light me-2"><a href="login.php" class="text-white text-decoration-none">Connexion</a></button>
                    <?php   
                        }
                    ?>
                </div>
            </nav>
        </div>
    </header><br><br><br><br>
    <div class="row g-5">
      <div class="col-md-5 col-lg-4 order-md-last">
        <h4 class="d-flex justify-content-between align-items-center mb-3">
          <span class="text-primary">Ajouter des fonds</span>
        </h4>
        <form action="" method="POST">
          <hr class="my-4">

          <h4 class="mb-3">Paiement</h4>

          <div class="my-3">
            <div class="form-check">
              <input id="credit" name="paymentMethod" type="radio" class="form-check-input" checked="" >
              <label class="form-check-label" for="credit">Credit card</label>
            </div>
            <div class="form-check">
              <input id="debit" name="paymentMethod" type="radio" class="form-check-input">
              <label class="form-check-label" for="debit">Binance account</label>
            </div>
            <div class="form-check">
              <input id="paypal" name="paymentMethod" type="radio" class="form-check-input" >
              <label class="form-check-label" for="paypal">PayPal</label>
            </div>
          </div>

          <div class="row gy-3">
            <div class="col-md-6">
              <label for="cc-name" class="form-label">Name on card</label>
              <input type="text" class="form-control" id="cc-name" placeholder="" >
              <small class="text-muted">Nom complet comme sur la carte</small>
              <div class="invalid-feedback">
                Nom requis
              </div>
            </div>

            <div class="col-md-6">
              <label for="cc-number" class="form-label">Credit card number</label>
              <input type="text" class="form-control" id="cc-number" placeholder="XXXXXXXXXXXXXXXXXXXX"maxlength="16" onkeypress="return onlyNumberKey(event)">
              <div class="invalid-feedback">
                Numéro de carte requis
              </div>
            </div>

            <div class="col-md-3">
              <label for="cc-expiration" class="form-label">Expiration</label>
              <input type="text" class="form-control" id="cc-expiration" placeholder="XX/XX" >
              <div class="invalid-feedback">
                Date d'expiration requise
              </div>
            </div>

            <div class="col-md-3">
              <label for="cc-cvv" class="form-label">CVV</label>
              <input type="text" class="form-control" id="cc-cvv" placeholder="XXX" maxlength="3" onkeypress="return onlyNumberKey(event)">
              <div class="invalid-feedback">
                CVV requis
              </div>
            </div>

            <div class="col-md-6">
              <label for="amount" class="form-label">Montant</label>
              <input type="text" name="submit_value" class="form-control" id="amount" placeholder="€" maxlength="10" onkeypress="return onlyNumberKey(event)">
              <div class="invalid-feedback">
                Montant requis
              </div>
              <h6>Solde actuel : <?php echo  $_SESSION['solde'];?></h6>
            </div>
          </div>

          <hr class="my-4">

          <button class="w-100 btn btn-primary btn-lg" name="submitbutton" type="submit">Valider le paiement</button>
        </form>
        <?php
        $value= $_POST['submit_value'];
        $submitbutton= $_POST['submitbutton'];

        if ($submitbutton){
            echo $value;
        }
        ?>
      </div>
    </div>
</body>
</html>
<script>
    function onlyNumberKey(evt) {
          
        // Only ASCII character in that range allowed
        var ASCIICode = (evt.which) ? evt.which : evt.keyCode
        if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
            return false;
        return true;
    }
</script>