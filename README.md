# Projet Labo Crypto

Groupe : Bourin Coco Peyrataud

# Sommaire

- [Sommaire](#Sommaire)
- [I - Descriptif du projet](#Descriptif-du-projet)
- [II - Site Web](#Site-Web)
- [III - Fonctionnalités](#Fonctionnalités)


# Descriptif du projet

Durant ce projet fil rouge nous avons pour but de:
- Créer un shop de NFT via un site web


# Site Web

Le site contiendra un shop, où on pourra trouver des NFT. Ces NFT pourrront être acheté avec la cryptomonnaie du site.

## Fonctionnalités

- Utilisateurs
- Ventes et achats de NFT 
- Monnaie du site pour acheter les NFT 



